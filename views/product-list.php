<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?= $pageData['title']; ?></title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="pb-2 mt-4 mb-2 border-bottom">
				<h1 class="display-4">Product List</h1>
			</div>
			<br>
			<div style="display:flex; justify-content:space-between; align-items:center;">
				<a class="btn btn-primary " href="/projects/scandiweb.com/products/create" role="button">Add product</a>
				<div class="form-row align-items-center">
					<div class="col-auto my-1">
						<div class="custom-control custom-checkbox mr-sm-2">
							<input type="checkbox" class="custom-control-input" id="check-all">
							<label class="custom-control-label" for="check-all">Select all</label>
						</div>
					</div>
					<div class="col-auto my-1">
						<button class="btn btn-danger" id="delete">Delete selected products</button>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
			<?php foreach ($pageData['products'] as $product) { ?>
				<div class="col-lg-3 col-md-4 col-sm-6 product">
					<div class="card bg-light mb-3 text-center">
						<div class="card-header">
							<div class="form-check">
								<input class="form-check-input position-static" type="checkbox" name="productsToDelete[]" value="<?=$product['id']?>">
							</div>
							<h5 class="my-0 font-weight-normal"><?= $product['sku']?></h5>
						</div>
						<div class="card-body">
							<h5 class="card-title"><?= $product['name']?></h5>
							<p class="card-text"><?= $product['price']?> &#36;</p>
							<p class="card-text"><?= $product['type_attr']?></p>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
	
	<script>
		$(document).ready(function(){
			$('#check-all').click(function(){
				if((this).checked) {
					$('.form-check-input').each(function(){
						(this).checked = true;
					});
				 } else {
					$('.form-check-input').each(function(){
						(this).checked = false;
					});
				}
			});
			
			$('#delete').click(function(){
				var productsToDelete = new Array();
				
				if($('.form-check input:checkbox:checked').length > 0) {
					$('.form-check input:checkbox:checked').each(function(){
						productsToDelete.push($(this).attr('value'));
						$(this).closest('.col-lg-3').remove();
					});
					sendResponse(productsToDelete);
				} else {
					alert('No data selected');
				}
				
				$('#check-all').prop('checked', false);
			});
			
			function sendResponse(productsToDelete){
				$.ajax({
					type:	"POST",
					url	:	"./deleteProducts",
					data:	{'productsToDelete': productsToDelete }
				});
			}
		});
	</script>
	</body>
</html>