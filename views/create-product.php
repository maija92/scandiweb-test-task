<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?= $pageData['title'];?></title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="pb-2 mt-4 mb-2 border-bottom">
				<h1 class="display-4">Create product</h1>
			</div><br>
			<form method="post" action="./createProduct">
			<?php if(isset($_GET['error'])) { ?>
				<div class="alert alert-warning alert-dismissible fade show" role="alert">This product already exists. Stock Keeping Unit <strong>(SKU)</strong> value must be unique.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php } ?>
				<div class="form-group row">
					<label for="sku" class="col-sm-2 col-form-label">SKU</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="sku" name="sku" placeholder="SKU" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="name" class="col-sm-2 col-form-label">Name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="price" class="col-sm-2 col-form-label">Price</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="price" name="price" placeholder="Price" pattern="^\d+(?:\.\d{1,2})?$" required>
						<div class="invalid-feedback">Please provide a valid price.</div>
					</div>
				</div>
				<div class="form-group row">
					<label for="productType" class="col-sm-2 col-form-label">Product type</label>
					<div class="col-sm-6">
						<select id="productType" class="form-control" name="type_name" required>
							<option></option>
							<?php foreach($pageData['types'] as $type) { ?>
							<option value="type<?=$type['id']?>"><?=$type['type']?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<?php foreach($pageData['types'] as $type) { ?>
				<div class="form-group row type<?php echo $type['id']?>" style="display:none;">
					<label for="type<?=$type['id']?>" class="col-sm-2 col-form-label"><?=$type['attribute']?></label>
					<div class="col-sm-6">
						<input type="text" class="form-control attribute" id="type<?=$type['id']?>" name="<?=$type['type']?>" placeholder="<?=$type['attribute']?>">
						<small class="form-text text-muted"><?=$type['note'];?></small>
					</div>
				</div>
				<?php } ?>
				<div class="form-group row">
					<div class="col-sm-6">
						<button type="submit" class="btn btn-primary create">Create</button>
						<a class="btn btn-secondary" href="./" role="button">Back</a>
					</div>
				</div>
			</form>	
			<script>
				$(document).ready(function() {
					var hideAll = function() {
						$('div[class*=type]').hide();
						$('.attribute').prop('value', '');
						$('.attribute').prop('required', false);
					}

					$('#productType').on('change', function() {
						hideAll();
						var category = $(this).val();

						if(category != '') {
							$('.' + category).show();
							$('#' + category).prop('required',true);
						}
					});
					
					$("#price").keyup(function () {
						if ($(this)[0].validity.valid == false) {
							$('.invalid-feedback').show();
						} else {
							$('.invalid-feedback').hide();
						}
					});
					
				});
			</script>	
		</div>
	</body>
</html>