<?php

class ProductsModel extends Model {
	
	public function getProducts() {
		$sql = "SELECT products.id, products.sku, products.name, products.price, products.type_attr FROM products ORDER BY products.id DESC";
		$stmt = $this->database->prepare($sql);
		$stmt->execute();
		$products = $stmt->fetchAll();

		return $products;
	}	
	
	public function getTypes() {
		$sql = "SELECT * FROM product_types";
		$stmt = $this->database->prepare($sql);
		$stmt->execute();
		$types = $stmt->fetchAll();

		return $types;
	}
	
	public function create($data) {
		$checkUnique = "SELECT * FROM `products` where `sku` = '$data->sku'";
		$unique = $this->database->prepare($checkUnique);
		$unique->execute();

		if ($unique->rowCount() > 0) {
			return $unique->rowCount();
		} else {
			$uniqueError = 'Ok';
			$sql = "INSERT INTO `products`(`sku`, `name`, `price`, `type_id`, `type_attr`) VALUES (:sku, :name, :price, :typeId, :typeAttribute)";
			$stmt = $this->database->prepare($sql);
			
			$stmt->bindValue(':sku', $data->sku, PDO::PARAM_STR);
			$stmt->bindValue(':name', $data->name, PDO::PARAM_STR);
			$stmt->bindValue(':price', $data->price, PDO::PARAM_STR);
			$stmt->bindValue(':typeId', $data->typeId, PDO::PARAM_INT);
			$stmt->bindValue(':typeAttribute', $data->typeAttribute, PDO::PARAM_STR);

			$stmt->execute();
		}
	}
	
	public function deleteProducts($data) {
		$products = implode(",",$data);
		$sql = "DELETE FROM `products` WHERE `id` IN ($products)";
		$stmt = $this->database->prepare($sql);
		$stmt->execute();
	}
}