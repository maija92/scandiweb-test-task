 <?php
include('Book.php');
include('Furniture.php');
include('DvdDisc.php');

abstract class Product {
	
	protected $sku;
	protected $name;
	protected $price;
	protected $typeId;
	protected $typeAttribute;
	
	protected static $types = [
        'type1' => DvdDisc::class,
        'type2' => Book::class,
		'type3' => Furniture::class
    ];
	
	public static function createProduct($type) {
        $product = new self::$types[$type];
		return $product;
    }
	
	public function __set($name,$value) {
		$functionname = 'set'.$name;
		return $this->$functionname($value);
	}
	
	public function __get($name) {
		$functionname = 'get'.$name;
		return $this->$functionname();
	}
	
	public function getSku() {
		return $this->sku;
	}
	public function setSku($sku) {
		$this->sku = $sku;
	}
	
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getPrice() {
		return $this->price;
	}
	public function setPrice($price) {
		$this->price = $price;
	}
	
	public function getTypeId() {
		return $this->typeId;
	}
	
	public function getTypeAttribute() {
		return $this->typeAttribute;
	}
	public function setTypeAttribute($attributeValue) {
		$this->typeAttribute = $attributeValue;
	}
	
	abstract protected function getTypeDescr($attributeValue);
}