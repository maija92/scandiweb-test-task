<?php
include('./models/Product.php');

class ProductsController extends BaseController {
	
	public function __construct() {
		$this->model = new ProductsModel;
		$this->view = new View;
	}
	
	public function index() {
		$this->pageData['title'] = "Products";
		$this->pageData['products'] = $this->model->getProducts();
		$this->view->render("/views/product-list.php", $this->pageData);
	}
	
	public function create() {
		$this->pageData['title'] = "Create product";
		$this->pageData['types'] = $this->model->getTypes();
		$this->view->render("/views/create-product.php", $this->pageData);
	}
	
	public function createProduct() {

		$product = Product::createProduct("{$_POST['type_name']}");
		$product->sku = $_POST['sku'];
		$product->name = $_POST['name'];
		$product->price = $_POST['price'];
		
		$types = $this->model->getTypes();
		
		foreach($types as $type) {
			if($_POST["{$type['type']}"] != '') {
				$product->typeAttribute = $product->getTypeDescr($_POST["{$type['type']}"]);
			}
		}
		$result = $this->model->create($product);

		if($result != null) {
			header("Location: ./create?error");
		} else {
			header("Location: /projects/scandiweb.com/products");
		}
		
	}
	
	public function deleteProducts() {
		$productsToDelete = $_POST['productsToDelete'];
		
		foreach($productsToDelete as $product){
			$products[] = $product;
		}
		var_dump($products);
		$this->model->deleteProducts($products);
	}
	
}