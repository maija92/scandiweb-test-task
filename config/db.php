<?php

class DB {
	const USERNAME = "root";
	const PASSWORD = "";
	const HOST = "localhost";
	const DATABASE = "scandiweb_products";
	
	public static function connectToDB() {
		$username = self::USERNAME;
		$password = self::PASSWORD;
		$host = self::HOST;
		$database = self::DATABASE;
		
		$connection = new PDO("mysql:dbname=$database;host=$host;charset=UTF8", $username, $password);
		return $connection;
	}
}