<?php

define("ROOT", $_SERVER['DOCUMENT_ROOT']);
define("CONTROLLER_PATH", ROOT."/projects/scandiweb.com/controllers/");
define("MODEL_PATH", ROOT."/projects/scandiweb.com/models/");
define("VIEW_PATH", ROOT."/projects/scandiweb.com/views/");

require_once("db.php");
require_once("route.php");
require_once(CONTROLLER_PATH. "BaseController.php");
require_once(MODEL_PATH. "Model.php");
require_once(VIEW_PATH. "View.php");

Routing::buildRoute();